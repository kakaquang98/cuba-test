package com.company.cuba.service;

import com.company.cuba.entity.Session;

import java.time.LocalDateTime;

public interface SessionService {
    String NAME = "cuba_SessionService";
    Session rescheduleSession(Session session, LocalDateTime newStartDate);

}