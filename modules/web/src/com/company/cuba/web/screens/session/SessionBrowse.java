package com.company.cuba.web.screens.session;

import com.company.cuba.service.SessionService;
import com.haulmont.cuba.gui.components.Calendar;
import com.haulmont.cuba.gui.components.EditorScreenFacet;
import com.haulmont.cuba.gui.model.CollectionContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.cuba.entity.Session;

import javax.inject.Inject;
import java.time.LocalDateTime;

@UiController("cuba_Session.browse")
@UiDescriptor("session-browse.xml")
@LookupComponent("sessionsTable")
@LoadDataBeforeShow
public class SessionBrowse extends StandardLookup<Session> {
//    @Inject
//    private EditorScreenFacet sessionEditDialog;
//    @Inject
//    private CollectionContainer<Session> sessionsDc;
//    @Inject
//    private SessionService sessionService;
//
//    @Subscribe("sessionsCalendar")
//    public void onSessionsCalendarCalendarEventClick(Calendar.CalendarEventClickEvent<LocalDateTime> event) {
//        sessionEditDialog.setEntityProvider( () -> event.getEntity());
//        sessionEditDialog.show();
//    }
//
//    @Subscribe("sessionsCalendar")
//    public void onSessionsCalendarCalendarEventMove(Calendar.CalendarEventMoveEvent<LocalDateTime> event) {
//        Session session = sessionService.rescheduleSession((Session) event.getEntity(), event.getNewStart());
//        sessionsDc.replaceItem(session);
//    }
}